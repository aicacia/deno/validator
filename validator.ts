export interface IError {
  message: string;
}

export interface ISchemaType {
  type?: string;
  description?: string;
}

export interface ISchemaDefinitions extends ISchemaType {
  definitions?: Record<string, ISchema>;
}

export type ISchemaWithDefinitions = ISchema & ISchemaDefinitions;

export type ISchema =
  | IOneOfSchema
  | IAnyOfSchema
  | IAllOfSchema
  | IRefSchema
  | INotSchema
  | IObjectSchema
  | IArraySchema
  | IBooleanSchema
  | INumberSchema
  | IStringSchema;

function isObject(value: any): value is Record<string, any> {
  return typeof value === "object" && value !== null;
}

export interface IStringSchema extends ISchemaType {
  type: "string";
  pattern?: string;
}

export function isStringSchema(value: any): value is IStringSchema {
  return isObject(value) && value.type === "string";
}

export interface INumberSchema extends ISchemaType {
  type: "number";
  format?: string;
}

export function isNumberSchema(value: any): value is INumberSchema {
  return isObject(value) && value.type === "number";
}

export type IBooleanSchema =
  | {
      type: "boolean";
    }
  | true
  | false;

export function isBooleanSchema(value: any): value is IBooleanSchema {
  return (
    value === true ||
    value === false ||
    (isObject(value) && value.type === "boolean")
  );
}

export interface IArraySchema extends ISchemaType {
  type?: "array";
  items?: ISchema;
  additionalItems?: ISchema;
}

export function isArraySchema(value: any): value is IArraySchema {
  return (
    isObject(value) && (value.type === "array" || value.hasOwnProperty("items"))
  );
}

export interface IObjectSchema extends ISchemaType {
  type?: "object";
  properties?: Record<string, ISchema>;
  patternProperties?: Record<string, ISchema>;
  additionalProperties?: boolean;
  required?: string[];
}

export function isObjectSchema(value: any): value is IObjectSchema {
  return (
    isObject(value) &&
    (value.type === "object" ||
      value.hasOwnProperty("properties") ||
      value.hasOwnProperty("patternProperties"))
  );
}

export interface IAnyOfSchema extends ISchemaType {
  anyOf: ISchema[];
}

export function isAnyOfSchema(value: any): value is IAnyOfSchema {
  return isObject(value) && Array.isArray((value as any).anyOf);
}

export interface IOneOfSchema extends ISchemaType {
  oneOf: ISchema[];
}

export function isOneOfSchema(value: any): value is IOneOfSchema {
  return isObject(value) && Array.isArray((value as any).oneOf);
}

export interface IAllOfSchema extends ISchemaType {
  allOf: ISchema[];
}

export function isAllOfSchema(value: any): value is IAllOfSchema {
  return isObject(value) && Array.isArray((value as any).allOf);
}

export interface INotSchema extends ISchemaType {
  not: ISchema;
}

export function isNotSchema(value: any): value is INotSchema {
  return isObject(value) && isObject((value as any).not);
}

export interface IRefSchema extends ISchemaType {
  $ref: string;
}

export function isRefSchema(value: any): value is IRefSchema {
  return isObject(value) && typeof (value as any).$ref === "string";
}

export function createValidator<T = any>(
  schema: ISchemaWithDefinitions
): IValidator<T> {
  const refs = schema.definitions ? createRefs(schema.definitions) : {},
    validatorInternal = createValidatorInternal(schema, refs);

  return function validator(value: any): value is T {
    const errors: IError[] = [],
      result = validatorInternal(value, errors);

    if (errors.length > 0) {
      throw errors;
    }
    return result;
  };
}

export function validate<T = any>(
  schema: ISchemaWithDefinitions,
  value: any
): value is T {
  return createValidator(schema)(value);
}

function createRefs(definitions: Record<string, ISchema>) {
  const refs: Record<string, IValidatorInternal> = {};

  for (const key of Object.keys(definitions)) {
    refs[`#/definitions/${key}`] = createValidatorInternal(
      definitions[key],
      refs
    );
  }

  return refs;
}

export type IValidator<T = any> = (value: any) => value is T;

type IValidatorInternal<T = any> = (value: any, errors: IError[]) => value is T;

function createValidatorInternal<T = any>(
  schema: ISchema,
  refs: Record<string, IValidatorInternal>
): IValidatorInternal<T> {
  if (isAnyOfSchema(schema)) {
    return createAnyOfValidatorInternal(schema, refs) as any;
  } else if (isAllOfSchema(schema)) {
    return createAllOfValidatorInternal(schema, refs) as any;
  } else if (isArraySchema(schema)) {
    return createArrayValidatorInternal(schema, refs) as any;
  } else if (isBooleanSchema(schema)) {
    return createBooleanValidatorInternal(schema) as any;
  } else if (isNumberSchema(schema)) {
    return createNumberValidatorInternal(schema) as any;
  } else if (isOneOfSchema(schema)) {
    return createOneOfValidatorInternal(schema, refs) as any;
  } else if (isNotSchema(schema)) {
    return createNotValidatorInternal(schema, refs) as any;
  } else if (isStringSchema(schema)) {
    return createStringValidatorInternal(schema) as any;
  } else if (isObjectSchema(schema)) {
    return createObjectValidatorInternal(schema, refs) as any;
  } else if (isRefSchema(schema)) {
    return createRefValidatorInternal(schema, refs) as any;
  } else if (isObject(schema)) {
    return createAnyValidatorInternal() as any;
  } else {
    throw new Error(`Invalid schema ${JSON.stringify(schema)}`);
  }
}

function createAnyValidatorInternal() {
  return function anyValidator() {
    return true;
  };
}

function createRefValidatorInternal(
  schema: IRefSchema,
  refs: Record<string, IValidatorInternal>
): IValidatorInternal {
  return refs[schema.$ref];
}

function createAnyOfValidatorInternal(
  schema: IAnyOfSchema,
  refs: Record<string, IValidatorInternal>
): IValidatorInternal<string> {
  const validators = schema.anyOf.map((schema) =>
    createValidatorInternal(schema, refs)
  );

  return function anyOfValidator(
    value: any,
    errors: IError[]
  ): value is string {
    for (const validator of validators) {
      if (validator(value, errors)) {
        return true;
      }
    }
    return false;
  };
}

function createOneOfValidatorInternal(
  schema: IOneOfSchema,
  refs: Record<string, IValidatorInternal>
): IValidatorInternal<string> {
  const validators = schema.oneOf.map((schema) =>
    createValidatorInternal(schema, refs)
  );

  return function anyOfValidator(
    value: any,
    errors: IError[]
  ): value is string {
    for (const validator of validators) {
      if (validator(value, errors)) {
        return true;
      }
    }
    return false;
  };
}

function createAllOfValidatorInternal(
  schema: IAllOfSchema,
  refs: Record<string, IValidatorInternal>
): IValidatorInternal<string> {
  const validators = schema.allOf.map((schema) =>
    createValidatorInternal(schema, refs)
  );

  return function allOfValidator(
    value: any,
    errors: IError[]
  ): value is string {
    for (const validator of validators) {
      if (!validator(value, errors)) {
        return false;
      }
    }
    return true;
  };
}

function createNotValidatorInternal(
  schema: INotSchema,
  refs: Record<string, IValidatorInternal>
): IValidatorInternal<string> {
  const validator = createValidatorInternal(schema.not, refs);

  return function anyOfValidator(
    value: any,
    errors: IError[]
  ): value is string {
    try {
      return !validator(value, errors);
    } catch (_error) {
      return false;
    }
  };
}

function createStringValidatorInternal(
  schema: IStringSchema
): IValidatorInternal<string> {
  return function stringValidator(
    value: any,
    errors: IError[]
  ): value is string {
    return typeof value === "string";
  };
}

function createNumberValidatorInternal(
  schema: INumberSchema
): IValidatorInternal<number> {
  return function numberValidator(
    value: any,
    errors: IError[]
  ): value is number {
    return typeof value === "number";
  };
}

function createBooleanValidatorInternal(
  schema: IBooleanSchema
): IValidatorInternal<boolean> {
  return function booleanValidator(
    value: any,
    errors: IError[]
  ): value is boolean {
    return typeof value === "boolean";
  };
}

function createArrayValidatorInternal<T = any>(
  schema: IArraySchema,
  refs: Record<string, IValidatorInternal>
): IValidatorInternal<T[]> {
  const itemsValidator =
    schema.items && createValidatorInternal(schema.items, refs);
  const additionalItemsValidator =
    schema.additionalItems &&
    createValidatorInternal(schema.additionalItems, refs);

  return function arrayValidator(array: any, errors: IError[]): array is T[] {
    if (Array.isArray(array) && array.length > 0) {
      if (itemsValidator) {
        for (const result of array.map((item) =>
          itemsValidator(item, errors)
            ? true
            : additionalItemsValidator && additionalItemsValidator(item, errors)
        )) {
          if (!result) {
            return false;
          }
        }
      }
      return true;
    } else {
      return false;
    }
  };
}

function createObjectValidatorInternal<T extends {} = {}>(
  schema: IObjectSchema,
  refs: Record<string, IValidatorInternal>
): IValidatorInternal<T> {
  const propertyValidators = {} as Record<
    keyof T,
    IValidatorInternal<T[keyof T]>
  >;
  const patternPropertyValidators = new Array<
    [RegExp, IValidatorInternal<T[keyof T]>]
  >();

  if (schema.properties) {
    for (const key of Object.keys(schema.properties)) {
      propertyValidators[key as keyof T] = createValidatorInternal(
        schema.properties[key],
        refs
      );
    }
  }
  if (schema.patternProperties) {
    for (const key of Object.keys(schema.patternProperties)) {
      patternPropertyValidators.push([
        new RegExp(key),
        createValidatorInternal(schema.patternProperties[key], refs),
      ]);
    }
  }

  return function objectValidator(object: any, errors: IError[]): object is T {
    if (typeof object === "object" && object !== null) {
      const keys = Object.keys(object) as Array<keyof T>;

      if (schema.required) {
        if (keys.length !== schema.required.length) {
          for (const key in schema.required) {
            if (!keys.includes(key as any)) {
              return false;
            }
          }
        }
      }

      for (const key of keys) {
        const value = object[key];

        if (!propertyValidators[key](value, errors)) {
          for (const [
            regExp,
            patternPropertyValidator,
          ] of patternPropertyValidators) {
            if (regExp.test(key as string)) {
              if (!patternPropertyValidator(value, errors)) {
                return false;
              }
            } else {
              return false;
            }
          }
        }
      }

      return true;
    } else {
      return false;
    }
  };
}
