import { createValidator } from "./validator.ts";
import { assert } from "https://deno.land/std@0.78.0/testing/asserts.ts";
import { walk } from "https://deno.land/std@0.78.0/fs/walk.ts";

for await (const entry of walk("suite/tests")) {
  if (entry.isFile) {
    const decoder = new TextDecoder("utf-8"),
      data = await Deno.readFile(entry.path),
      testsJSON = JSON.parse(decoder.decode(data));

    for (const testJSON of testsJSON) {
      const validator = createValidator(testJSON.schema);

      for (const test of testJSON.tests) {
        Deno.test(
          `${entry.path} ${testJSON.description} ${test.description}`,
          () => {
            assert(validator(test.data) === test.valid, test.description);
          }
        );
      }
    }
  }
}
