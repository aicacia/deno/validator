import { IObjectSchema } from "./IObjectSchema.ts";

export type ISchema<T = any> = boolean | IObjectSchema<T>;
