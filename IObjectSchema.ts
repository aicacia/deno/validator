import { isObject } from "./isObject.ts";
import { ISchema } from "./ISchema.ts";

export interface IObjectSchema<T extends Record<string, any> = Record<string, any>> {
  type?: "object";
  properties?: Record<keyof T, ISchema<T[keyof T]>>;
  patternProperties?: Record<string, T[any]>;
  additionalProperties?: boolean;
  required?: keyof T[];
}

export function isObjectSchema(value: any): value is IObjectSchema {
  return (
    isObject(value) &&
    (value.type === "object" ||
      value.hasOwnProperty("properties") ||
      value.hasOwnProperty("patternProperties") ||
      value.hasOwnProperty("additionalProperties") ||
      value.hasOwnProperty("required"))
  );
}
